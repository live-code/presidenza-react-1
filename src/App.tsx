import React, { Suspense }  from 'react';
import './App.css';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { DemoPanel } from './pages/demo/DemoPanel';
import { NavBar } from './core/NavBar';
import { DemoControlledForm } from './pages/demo/DemoControlledForm';
import { DemoList } from './pages/demo/DemoList';
import { DemoUncontrolledForm } from './pages/demo/DemoUncontrolledForm';
import { DemoHook1 } from './pages/demo/DemoHook1';
import { DemoListDetails } from './pages/demo/DemoListDetails';
// import { UserPage } from './pages/user/UserPage';
const UserPage = React.lazy(() => import('./pages/user/UserPage'));


// pass params
// nested routes
// lazy loading

function App() {
  return (
    <BrowserRouter>
      <NavBar />
      <br/>
      <br/>
      <Suspense fallback={<div>Loading....</div>}>
        <Switch>
          <Route path="/home">
            <DemoPanel />
          </Route>
          <Route path="/user" component={UserPage} />
          <Route path="/demohook1" component={DemoHook1} />
          <Route path="/demolist" component={DemoList} exact />
          <Route path="/demolist/:id" component={DemoListDetails} />
          <Route path="/uform" component={DemoUncontrolledForm} />
          <Route path="/cform" component={DemoControlledForm} />
          <Route path="*">
            <Redirect to="/home" />
          </Route>
        </Switch>
      </Suspense>
    </BrowserRouter>
  );
}

export default App;







