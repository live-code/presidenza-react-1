import React, { useState } from 'react';
import './App.css';
import { DemoPanel } from './pages/demo/DemoPanel';
import UserPage from './pages/user/UserPage';
import { DemoHook1 } from './pages/demo/DemoHook1';
import { DemoList } from './pages/demo/DemoList';
import { DemoUncontrolledForm } from './pages/demo/DemoUncontrolledForm';
import { DemoControlledForm } from './pages/demo/DemoControlledForm';

function App() {
  const [section, setSection] = useState<string>('demolist');

  function renderPage() {
    switch (section) {
      case 'demo': return <DemoPanel />;
      case 'user': return <UserPage />;
      case 'demohook1': return <DemoHook1/>;
      case 'demolist': return <DemoList/>;
      case 'uform': return <DemoUncontrolledForm/>;
      case 'cform': return <DemoControlledForm/>;
    }
  }

  return (
    <div>
      <button onClick={() => setSection('uform')}>U-Forms</button>
      <button onClick={() => setSection('cform')}>C-Forms</button>
      <button onClick={() => setSection('demolist')}>Demo List</button>
      <button onClick={() => setSection('demohook1')}>Demo Hook</button>
      <button onClick={() => setSection('user')}>User</button>
      <button onClick={() => setSection('demo')}>Demo Panel</button>
      <hr/>
      {renderPage()}
    </div>
  );
}

export default App;







