import React  from 'react';
import { NavLink } from 'react-router-dom';

export const NavBar: React.FC = () => {
  return <div className="btn-group">
    <NavLink className="btn btn-primary" to="/home" activeClassName="bg-dark">home</NavLink>
    <NavLink className="btn btn-primary" to="/user" activeClassName="bg-dark">user</NavLink>
    <NavLink className="btn btn-primary" to="/demohook1" activeClassName="bg-dark">demohook1</NavLink>
    <NavLink className="btn btn-primary" to="/demolist" activeClassName="bg-dark">demolist</NavLink>
    <NavLink className="btn btn-primary" to="/uform" activeClassName="bg-dark">uform</NavLink>
    <NavLink className="btn btn-primary" to="/cform" activeClassName="bg-dark">cform</NavLink>

  </div>
};
