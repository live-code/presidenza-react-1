export interface Friend {
  id: number;
  name: string;
  tweets: number;
}
