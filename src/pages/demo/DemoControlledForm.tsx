import React, { useState } from 'react';
import cn from 'classnames';

interface FormData {
  user: string;
  pass:string;
}
const initialState = { user: '', pass: ''};

export const DemoControlledForm: React.FC = () => {
  const [formData, setFormData] = useState<FormData>(initialState)
  const [dirty, setDirty] = useState<boolean>(false)

  function onChangeFormData(e: React.ChangeEvent<HTMLInputElement>) {
    setFormData({
      ...formData,
      [e.currentTarget.name]: e.currentTarget.value,
    });
  }

  function submitHandler(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    console.log('POST', formData)
    setFormData(initialState);
    setDirty(false)
  }

  const isUserInvalid = formData.user === '';
  const isPassInvalid = formData.pass === '';
  const invalid = isUserInvalid || isPassInvalid;

  return (
    <div>
      <form onSubmit={submitHandler}>
        <input
          className={cn('form-control', { 'is-invalid': isUserInvalid && dirty, 'is-valid': !isUserInvalid  } )}
          type="text" value={formData.user} onChange={onChangeFormData} name="user"/>
        <input
          className={cn('form-control', { 'is-invalid': isPassInvalid && dirty, 'is-valid': !isPassInvalid  } )}
          type="text" value={formData.pass} onChange={onChangeFormData} name="pass"/>
        <button type="submit" disabled={invalid}>SEND</button>
      </form>
    </div>
  )
};
