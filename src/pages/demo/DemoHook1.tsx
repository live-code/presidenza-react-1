import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { User } from '../../model/user';

export const DemoHook1: React.FC = () => {
  const [id, setId] = useState<number>(2)
  const [title, setTitle] = useState<string>('User DETAILS')
  return <div>
    <button onClick={() => setId(1)}>1</button>
    <button onClick={() => setId(2)}>2</button>
    <button onClick={() => setId(3)}>3</button>
    <button onClick={() => setTitle('Details!!!!')}>Change Title</button>
    <hr/>
    <UserDetails userId={id} title={title} />
  </div>
};

// =======
interface UserDetailsProps {
  userId: number;
  title: string;
}
export const UserDetails: React.VFC<UserDetailsProps> = (props) => {
  const [user, setUser] = useState<User | null>(null);

  useEffect(() => {
    console.log('INIT')
  }, []);

  useEffect(() => {
    console.log('USE EFFECT USER ID')
    axios.get<User>('https://jsonplaceholder.typicode.com/users/' + props.userId)
      .then(res => setUser(res.data))
  }, [props.userId])

  useEffect(() => {
    console.log('USE EFFECT TITLE')
  }, [props.title])

  console.log('render')
  return <div>{props.title}: {user?.name}</div>
}
