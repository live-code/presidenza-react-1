import React, { useEffect, useState } from 'react';
import { Friend } from '../../model/friend';
import axios from 'axios';
import { ListItem } from './DemoListItem';


export const DemoList: React.FC = () => {
  const [friends, setFriends] = useState<Friend[] | null>(null);
  const [selectedFriend, setSelectedFriend] = useState<Friend | null>(null);

  const [formData, setFormData] = useState<Partial<Friend>>({ name: '', tweets: 0})

  useEffect(() => {
    axios.get<Friend[]>('http://localhost:3001/friends')
      .then(res => setFriends(res.data))
  }, []);

  function onChangeFormData(e: React.ChangeEvent<HTMLInputElement>) {
    const isNumber = e.currentTarget.type === 'number';
    setFormData({
      ...formData,
      [e.currentTarget.name]: isNumber ? +e.currentTarget.value : e.currentTarget.value
    })
  }

  function deleteHandler(id: number) {
    if (friends) {
      axios.delete('http://localhost:3001/friends/' + id)
        .then(() => {
          setFriends(friends.filter(f => f.id !== id))
        })
    }
  }
  function submitHandler(e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault();
    axios.post<Friend>('http://localhost:3001/friends', formData)
      .then(res => {
          // setFriends(friends ? [...friends, res.data] : [res.data], );
          setFriends([...friends || [], res.data]);
      })
  }

  return (
    <div>

      {/* Form */}
      <form onSubmit={submitHandler}>
        <input type="text" name="name" value={formData.name} onChange={onChangeFormData}/>
        <input type="number" name="tweets" value={formData.tweets}  onChange={onChangeFormData} />
        <button type="submit">SEND</button>
      </form>

      {/* List */}
      {!friends && <div>loading...</div>}
      {friends?.length === 0 && <div>There are no items</div>}
      <div>
        {friends?.map((friend: Friend) => {
          return <ListItem
            friend={friend}
            key={friend.id}
            selected={friend.id === selectedFriend?.id}
            onDeleteFriend={() => deleteHandler(friend.id)}
            onSelectFriend={() => setSelectedFriend(friend)}
          />
        })}
      </div>
      <Total friends={friends} />

      <hr />

      {
        selectedFriend && (
          <div>
            <h1>{selectedFriend.name}</h1>
            <h1>{selectedFriend.tweets}</h1>
          </div>
        )
      }

    </div>
  )
};

// TotalFriends.tsx
export const Total: React.FC<{ friends: Friend[] | null }> = (props) => {
  const getTotal = () => props.friends?.reduce((acc, f) => acc + f.tweets, 0);

  return <div className="text-center">
    {getTotal()}
  </div>
}
