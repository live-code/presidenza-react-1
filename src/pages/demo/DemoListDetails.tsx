import React, { useEffect, useState } from 'react';
import { NavLink, useParams } from 'react-router-dom';
import axios from 'axios';
import { Friend } from '../../model/friend';

type RouterParams = {
  id: string;
}

export const DemoListDetails: React.VFC = () => {
  const [friend, setFriend] = useState<Friend | null>(null);
  const [error, setError] = useState<boolean>(false)
  const { id } = useParams<RouterParams>()

  useEffect(() => {
    axios.get<Friend>('http://localhost:3001/friends/' + id)
      .then(res => setFriend(res.data))
      .catch(err => setError(true))
  }, [id]);

  return <div>
    {
      error ?
        <div className="alert">User does not exist</div> :
        <div>{friend?.name} has tweeted {friend?.tweets} tweets</div>
    }

     <NavLink to="/demolist">Back to list</NavLink>

  </div>
};
