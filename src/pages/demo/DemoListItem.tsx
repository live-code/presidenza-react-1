import { Friend } from '../../model/friend';
import React, { useState } from 'react';
import cn from 'classnames';
import { NavLink } from 'react-router-dom';

interface ListItemProps {
  friend: Friend;
  selected: boolean;
  onDeleteFriend: () => void;
  onSelectFriend: () => void;
}
export const ListItem: React.FC<ListItemProps> = (props) => {
  const { friend, selected, onDeleteFriend, onSelectFriend} = props;
  const [ opened, setOpened ] = useState<boolean>(false);

  function selectFriendHandler(e: React.MouseEvent) {
    e.stopPropagation();
    onSelectFriend();
  }
  
  function deleteFriendHandler(e: React.MouseEvent) {
    e.stopPropagation();
    onDeleteFriend();
  }

  return <li
    onClick={() => setOpened(!opened)}
    className={cn('list-group-item', { active: selected})}
  >
    {friend.name}
    <div className="pull-right">
      <div className="badge badge-dark">
        {friend.tweets}
      </div>
      <NavLink to={'demolist/' + friend.id }>
        <i className="fa fa-link ml-2" />
      </NavLink>
      <i className="fa fa-info-circle ml-2" onClick={selectFriendHandler} />
      <i className="fa fa-trash ml-2" onClick={deleteFriendHandler} />
    </div>

    {opened && <div>
      {friend.id} - {friend.name} - { friend.tweets }
    </div>}
  </li>
};

