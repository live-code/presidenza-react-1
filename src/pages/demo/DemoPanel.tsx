import React, { useState } from 'react';
import cn from 'classnames';

export const DemoPanel: React.FC = () => {
  const [showModal, setShowModal] = useState<boolean>(false);

  const goto = () => {
    window.open('http://www.google.com')
  }
  return <div>
    <Panel
      title="Profile"
      theme="dark"
      icon="fa fa-info-circle"
      onIconClick={() => setShowModal(true)}
      styles={ { marginBottom: 50} }
    > bla bla bla ....</Panel>

    <Panel
      title="Profile"
      styles={{ borderRadius: 50}}
    > bla bla bla ....</Panel>

    <Panel icon="fa fa-google" onIconClick={goto}> empty panel</Panel>

    {showModal && <div style={{ position: 'fixed', top: 0, bottom: 0, left: 0, right: 0, backgroundColor: 'blue' }}>
      hello
      <button onClick={() => setShowModal(false)}>close</button>
    </div>}
  </div>
};


// =============================
interface PanelProps {
  title?: string;
  theme?: 'dark' | 'light',
  styles?: React.CSSProperties;
  icon?: string;
  onIconClick?: () => void
}

export const Panel: React.FC<PanelProps> = ({ onIconClick, icon, theme, title, styles, children }) => {
  const [open, setOpen] = useState<boolean>(true);
  const css = { border: '3px solid black', ...styles };

  function onIconClickHandler(event: React.MouseEvent) {
    event.stopPropagation();
    if (onIconClick) {
      onIconClick();
    }
  }

  return (
    <div className="card" style={css}>
      <div onClick={() => setOpen(!open)} className={cn(
        'card-header',
        { 'bg-dark text-white': theme === 'dark' },
        { 'bg-light': theme === 'light' },
      )}>
        {title}
        {icon && <i className={cn(icon, 'pull-right')} onClick={onIconClickHandler}/>}
      </div>

      {open && <div className="card-body">
        {children}
      </div>}
    </div>
  )
}
