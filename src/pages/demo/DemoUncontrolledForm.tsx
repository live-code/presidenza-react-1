import React, { useEffect, useRef } from 'react';

export const DemoUncontrolledForm: React.FC = () => {
  const inputEl = useRef<HTMLInputElement>(null);
  const input2El = useRef<HTMLInputElement>(null);

  useEffect(() => {
    inputEl.current?.focus()
  }, [])

  function submit() {
    if (inputEl.current && input2El.current) {
      if (inputEl.current?.value !== '') {
        console.log(inputEl.current?.value, input2El.current?.value)
        inputEl.current.value = '';
        input2El.current.value = ''
      }
    }
  }
  return <div>
    <input type="text" ref={inputEl} />
    <input type="text" ref={input2El} />
    <button onClick={submit}>SUBMIT</button>
  </div>
};
