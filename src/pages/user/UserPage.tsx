import React from 'react';
import { GMap } from './components/Gmap';
import { UserTitle } from './components/UserTitle';
import { useUser } from './hooks/useUser';
import { NgIf } from '../../shared/NgIf'


export default function UserPage() {
  const { user, setUser } = useUser();

  return (
    <div >
      <button onClick={() => setUser(null)}>unload</button>
      <div className="card">
        <div className="card card-header">Profile</div>
        <NgIf value={!!user}>
         <div className="card card-body">
            <UserTitle user={user}/>
            <img src={user?.picture.large} alt=""/>
            <GMap city={user?.location.city || ''}/>
            <a href={'mailto:' + user?.email}>Send Email</a>
          </div>
        </NgIf>
      </div>
    </div>
  );
}
