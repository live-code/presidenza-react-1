import React from 'react';

interface GmapProps {
  city: string;
}
export const GMap = (props: GmapProps) => {
  const mapURL = 'https://maps.googleapis.com/maps/api/staticmap?center=' + props.city + '&zoom=5&size=200x100&key=AIzaSyDSBmiSWV4-AfzaTPNSJhu-awtfBNi9o2k';
  return <img src={mapURL} alt=""/>
}
