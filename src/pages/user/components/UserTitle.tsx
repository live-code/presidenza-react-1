
// ----
// UserTitle.tsx
import { Result } from '../../../model/random-user';
import React from 'react';

interface UserTitleProps {
  user: Result | null;
}
export const UserTitle = (props: UserTitleProps) => {
  const displayName = props.user ? `${props.user?.name.title} ${props.user?.name.first} ${props.user?.name.last}` : '';
  const color = props.user?.gender === 'male' ? 'blue' : 'pink';
  return <h1 style={{ color }}>{displayName}</h1>
}
