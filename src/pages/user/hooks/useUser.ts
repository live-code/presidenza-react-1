import { useEffect, useState } from 'react';
import axios from 'axios';
import { RandomUser, Result } from '../../../model/random-user';

export function useUser() {
  const [user, setUser] = useState<Result | null>(null);

  useEffect(() => {
    axios.get<RandomUser>('https://randomuser.me/api')
      .then(res => {
        setUser(res.data.results[0]);
      })
  }, [])


  return { user, setUser };
}
