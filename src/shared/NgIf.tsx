import React from 'react';

export const NgIf: React.FC<{value: boolean}> = (props) => {
  return <div>
    {props.value ? props.children : null}
  </div>
}
