export function getCls(gender: string) {
  switch(gender) {
    case 'M':
      return 'centered male';
    default:
    case 'F':
      return 'centered female';
  }
}
